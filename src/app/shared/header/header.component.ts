import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor() { }
  @Output() changeLang: EventEmitter<any> = new EventEmitter<any>();
  ngOnInit() {
  }
  /**Method to initiate the language change */
  public changeLanguage(language): void {
    this.changeLang.emit(language);
  }

}

import { Component, OnInit, Input, ChangeDetectorRef, SimpleChanges, TemplateRef, Output, EventEmitter } from '@angular/core';
import { SelectItem } from 'primeng/api/selectitem';
import { catchError } from 'rxjs/operators';
import { FormGroup } from '@angular/forms';

@Component({
    selector: 'app-paginator',
    templateUrl: './paginator.component.html',
    styleUrls: ['./paginator.component.scss']
})
export class PaginatorComponent implements OnInit {
    @Input() pageLinkSize: number = 5;

    // tslint:disable-next-line: no-output-on-prefix
    @Output() onPageChange: EventEmitter<any> = new EventEmitter();

    @Output() filterData: EventEmitter<any> = new EventEmitter();

    @Output() downloadCSVfile: EventEmitter<any> = new EventEmitter();

    @Output() refreshOnclick: EventEmitter<any> = new EventEmitter();

    @Input() form: FormGroup;

    @Input() text: any;

    @Input() style: any;

    @Input() styleClass: string;

    @Input() alwaysShow: boolean = true;

    @Input() templateLeft: TemplateRef<any>;

    @Input() templateRight: TemplateRef<any>;

    @Input() dropdownAppendTo: any;

    @Input() dropdownScrollHeight: string = '200px';

    @Input() currentPageReportTemplate: string = '{currentPage} of {totalPages}';

    @Input() showCurrentPageReport: boolean;

    @Input() totalRecords: number;

    @Input() rows: number;

    @Input() rowsPerPageOptions: any[];

    @Input() loadingState: boolean;

    pageLinks: number[];

    rowsPerPageItems: SelectItem[];

    paginatorState: any;

    _first: number = 0;
    http: any;
    

    constructor(private cd: ChangeDetectorRef) { }

    ngOnInit() {
        this.updatePaginatorState();
    }
    /** Methods under change cycle */
    ngOnChanges(simpleChange: SimpleChanges) {

        if (simpleChange.totalRecords) {
            this.updatePageLinks();
            this.updatePaginatorState();
            this.updateFirst();
            this.updateRowsPerPageOptions();
        }

        if (simpleChange.first) {
            this._first = simpleChange.first.currentValue;
            this.updatePageLinks();
            this.updatePaginatorState();
        }

        if (simpleChange.rows) {
            this.updatePageLinks();
            this.updatePaginatorState();
        }

        if (simpleChange.rowsPerPageOptions) {
            this.updateRowsPerPageOptions();
        }
    }

    @Input() get first(): number {
        return this._first;
    }
    set first(val: number) {
        this._first = val;
    }
    /**Defining Rows per page as per the selection */
    private updateRowsPerPageOptions(): void {
        if (this.rowsPerPageOptions) {
            this.rowsPerPageItems = [];
            for (let opt of this.rowsPerPageOptions) {
                if (typeof opt == 'object' && opt['showAll']) {
                    this.rowsPerPageItems.push({ label: opt['showAll'], value: this.totalRecords });
                }
                else {
                    this.rowsPerPageItems.push({ label: String(opt), value: opt });
                }
            }
        }
    }
    /**confirmation the pages  */
    public isFirstPage(): boolean {
        return this.getPage() === 0;
    }

    public isLastPage(): boolean {
        return this.getPage() === this.getPageCount() - 1;
    }
    /**Getting page count */
    public getPageCount(): number {
        return Math.ceil(this.totalRecords / this.rows) || 1;
    }
    /**Getting start and end of the pages */
    private calculatePageLinkBoundaries(): Array<any> {
        let numberOfPages = this.getPageCount(),
            visiblePages = Math.min(this.pageLinkSize, numberOfPages);

        //calculate range, keep current in middle if necessary
        let start = Math.max(0, Math.ceil(this.getPage() - ((visiblePages) / 2))),
            end = Math.min(numberOfPages - 1, start + visiblePages - 1);

        //check when approaching to last page
        var delta = this.pageLinkSize - (end - start + 1);
        start = Math.max(0, start - delta);

        return [start, end];
    }

    /**Updating the page data on change event triggering */

    private updatePageLinks(): void {
        this.pageLinks = [];
        let boundaries = this.calculatePageLinkBoundaries(),
            start = boundaries[0],
            end = boundaries[1];

        for (let i = start; i <= end; i++) {
            this.pageLinks.push(i + 1);
        }
    }
    /**Methods for identifying and updating the page the page */

    private changePage(p: number): void {
        var pc = this.getPageCount();

        if (p >= 0 && p < pc) {
            this._first = this.rows * p;
            var state = {
                page: p,
                first: this.first,
                rows: this.rows,
                pageCount: pc
            };
            this.updatePageLinks();
            this.onPageChange.emit(state);
            this.updatePaginatorState();
        }
    }

    private updateFirst(): void {
        const page = this.getPage();
        if (page > 0 && this.totalRecords && (this.first >= this.totalRecords)) {
            Promise.resolve(null).then(() => this.changePage(page - 1));
        }
    }

    public getPage(): number {
        return Math.floor(this.first / this.rows);
    }

    public changePageToFirst(event: any): void {
        if (!this.isFirstPage()) {
            this.changePage(0);
        }

        event.preventDefault();
    }

    public changePageToPrev(event: any): void {
        this.changePage(this.getPage() - 1);
        event.preventDefault();
    }

    public changePageToNext(event: any): void {
        this.changePage(this.getPage() + 1);
        event.preventDefault();
    }

    public changePageToLast(event: any): void {
        if (!this.isLastPage()) {
            this.changePage(this.getPageCount() - 1);
        }

        event.preventDefault();
    }

    public onPageLinkClick(event: any, page: number): void {
        this.changePage(page);
        event.preventDefault();
    }

    public onRppChange(event: any): void {
        this.changePage(this.getPage());
    }

    /**updating the paginator */

    private updatePaginatorState(): void {
        this.paginatorState = {
            page: this.getPage(),
            pageCount: this.getPageCount(),
            rows: this.rows,
            first: this.first,
            totalRecords: this.totalRecords
        }
    }


    private get currentPageReport(): string {
        return this.currentPageReportTemplate
            .replace("{currentPage}", (this.getPage() + 1).toString())
            .replace("{totalPages}", this.getPageCount().toString());
    }

    /**Method for filtering universally in table */

    public filter1(event: any): void {
        this.filterData.emit(event);
    }
    /**Method for CSV download */
    public downloadCSV(): void {
        this.downloadCSVfile.emit();
    }
    /**Method for reloading original values */
    public reload(): void {
        this.refreshOnclick.emit();

    }
}


